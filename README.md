Written using Python 3.7.3

# Usage

1. Configure MySQL db locally, or remotely with remote access enabled
2. Edit `config.ini` to set appropriate variables
3. Build the docker image with `docker build . -t cms-loader`
4. Run the image, providing a mount flag for access to the input file: `docker run cms-loader -v {input_file_path}:/usr/src/{input_file_name}`

# Notes

- If running against a local database:
  - On linux, add the `--network="host"` flag to the `docker run` command, and use `127.0.0.1` as the host in the connection string
  - On Windows/Mac, use `host.docker.internal` as the host in the connection string