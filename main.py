import csv
from sqlalchemy import MetaData, Table, Column, create_engine, String, Float
import mysql.connector
import configparser
import sys
import time

def prepare_engine(connection_string):
    engine = create_engine(connection_string)

    try:
        engine.connect()
        return engine
    except:
        exit("Unable to connect to %s" % connection_string)

def prepare_table(engine, fields, table_name, payment_field):
    metadata = MetaData()
    table = Table(table_name, metadata)

    # Drop table if it already exists
    if engine.dialect.has_table(engine, table_name):
        table.drop(engine)

    # Generate table columns from user configuration
    for field in fields:
        data_type = Float() if field == payment_field else String(255)
        table.append_column(Column(field, data_type))

    table.create(engine)
    return table

def process_infile(filename, batchsize, fields, table, engine):
    rows_processed = 0
    batch_count = 0
    working_set = list()
    start_time = time.time()

    with open(filename, newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            batch_count += 1
            working_set.append({ key: row[key] for key in fields })
            if batch_count == batchsize:
                # Insert batch-size rows at a time
                rows_processed = insert_rows(working_set, rows_processed, start_time, table, engine)
                working_set = list()
                batch_count = 0            
        
        # Insert any remaining rows (row_count % batch_size > 0)
        insert_rows(working_set, rows_processed, start_time, table, engine)

def insert_rows(working_set, rows_processed, start_time, table, engine):
    # Insert batchsize rows at a time
    engine.execute(table.insert(), working_set)
    
    # Display progress
    rows_processed += len(working_set)
    elapsed_time = time.time() - start_time
    elapsed_display = time.strftime("%H:%M:%S", time.gmtime(elapsed_time))
    sys.stdout.write("\rElapsed time: %s Rows processed: %s" % (elapsed_display, rows_processed))
    sys.stdout.flush()

    return rows_processed

if __name__ == "__main__":
    parser = configparser.ConfigParser()
    parser.read('config.ini')
    config = parser['DEFAULT']

    infile = config['inputfile']
    table_name = config['tablename']
    payment_field = config['paymentfield']
    fields = config['fields'].split(',')
    batchsize = config.getint('batchsize')

    print("Connecting to database at %s" % config['connectionstring'])
    engine = prepare_engine(config['connectionstring'])

    print("Preparing table: %s" % table_name)
    table = prepare_table(engine, fields, table_name, payment_field)

    print("Processing input file: %s\nInserting fields: %s\nPayment field: %s\nRows per batch: %s\n" % (infile, ', '.join(fields), payment_field, batchsize))    
    process_infile(infile, batchsize, fields, table, engine)