FROM python

# Install dependencies
RUN pip install sqlalchemy mysql-connector

# Copy source over
RUN mkdir -p /usr/src
WORKDIR /usr/src
ADD main.py /usr/src
ADD config.ini /usr/src

# Kick off cronjob
CMD [ "python", "./main.py" ]